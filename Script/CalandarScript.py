﻿from time import gmtime, strftime
import datetime,time
import json
import os,sys

def Test1():
  # Get Test data
  with open(os.getcwd()+'\\InputJson\\InputDatas.json') as rd:
      inputDatas = json.load(rd)
  rd.close()
  # Get Time
  Timenowwithformated = time.strftime('%H:%M')
  Log.Message(Timenowwithformated)
  timenow = datetime.datetime.now()
  now_plus_30 = timenow + datetime.timedelta(minutes = 30)
  timeafter_30 = now_plus_30.strftime('%H:%M')
  Log.Message(timeafter_30)
  # Search Calendar app in Windows Start 
  Aliases.windowsbutton.Taskbar.Keys("[Win]")
  WindowsStartPage = Aliases.StartPage
  SearchInputBox = WindowsStartPage.BrowserWindow.Search_box
  NameMapping.Sys.Keys("[Hold][Win]s[Release]")
  SearchInputBox.Keys("calendar")
  WindowsStartPage.page.Click(248, 136)
  
  # Calendar App Navigation
  MainPageWindow = Aliases.WindowsCalendarApp.AppProperties.MainWindow
  MainPageWindow.New_event.Click()
  EventWindowProperties = MainPageWindow.EventCreation.EventWindow
  EventWindowProperties.Event_name.Keys(inputDatas["EventName"])
  EventWindowProperties.StartDatePicker.Click()
  
  #start and end date selection
  MainPageWindow.DateSelector.CalendarPopup.CalenderDate.InputStartDate.Click()
  EventWindowProperties.EndDatePicker.Click()
  aqUtils.Delay(2000)
  MainPageWindow.DateSelectorEnddate.CalendarPopup.CalenderEndDate.InputEndDate.Click()
  
  # start and end time selection
  StartTimetextBox = EventWindowProperties.MeetingStartTime.MeetingStartTimeChild
  StartTimetextBox.ContentElement.Click(117, 19)
  StartTimetextBox.Keys(Timenowwithformated)
  
  EndTimetextBox = EventWindowProperties.MeetingEndTime.MeetingEndTimeChild
  EndTimetextBox.ContentElement.Click(85, 11)
  EndTimetextBox.Keys(timeafter_30)
  aqUtils.Delay(2000)
  MainPageWindow.EventPageHeaders.HeaderOptionOne.HeaderOptionTwo.Home.SelectActions.Actions.Save.Click()
  aqUtils.Delay(2000)
  MainPageWindowProperties = MainPageWindow.MainWindowHeaders.Show
  
  OCR.Recognize(MainPageWindowProperties.Week).BlockByText("Week").Click()
  MainPageWindow.SelectCreatedEvent.TodayDateEventSelect.SelectCurrentDateEvent.DblClick()
  
  # Update the event
  EventWindowProperties.Event_name.Keys("^a[BS]"+inputDatas["UpdatedEventname"])
  EventWindowProperties.StartDatePicker.Click()
  MainPageWindow.DateSelector.CalendarPopup.CalenderDate.InputStartDate.Click()
  EventWindowProperties.EndDatePicker.Click()
  aqUtils.Delay(2000)
  MainPageWindow.DateSelectorEnddate.CalendarPopup.CalenderEndDate.InputEndDate.Click()
  StartTimetextBox = EventWindowProperties.MeetingStartTime.MeetingStartTimeChild
  StartTimetextBox.ContentElement.Click(117, 19)
  StartTimetextBox.Keys(Timenowwithformated)
  
  EndTimetextBox = EventWindowProperties.MeetingEndTime.MeetingEndTimeChild
  EndTimetextBox.ContentElement.Click(85, 11)
  EndTimetextBox.Keys(timeafter_30)
  
  MainPageWindow.EventPageHeaders.HeaderOptionOne.HeaderOptionTwo.Home.SelectActions.Actions.Save.Click()
  # Delete event
  MainPageWindow.SelectCreatedEvent.TodayDateEventSelect.SelectCurrentDateEvent.DblClick()
  MainPageWindow.EventPageHeaders.HeaderOptionOne.HeaderOptionTwo.Home.SelectActions.Actions.Delete.Click()
  
  MainPageWindow.Delete.DeletePopUp.ClickDelete.DeleteButton.Click()
  MainPageWindowProperties.Month.Click()
  Aliases.ApplicationFrameHost.wndApplicationFrameWindow.Close()
